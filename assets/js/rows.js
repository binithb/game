var _ = require('lodash');
var React = require('react');
var React = require('react');
import Paper from 'material-ui/Paper';


export default class HorizontalPaper extends React.Component {
    constructor(props) {
        console.log("constructing horizontal paper ");
        super(props);
        console.log("figure list " + props.figure_list);
        let figure_list_length = props.figure_list.length;
        console.log("figure list length " + figure_list_length);
        this.state = {no_of_rows: figure_list_length };
        console.log("number of rows " + this.state.no_of_rows);
        console.log("dispatcher == " + this.props.dispatcher);
    }

    get_styles = () => {
        return ({
            display: 'flex',
            flexDirection: 'row',
            flex: 1,
            background: 'green'
        })
    }

    render = () => {
        let rows_list = [];

        _.range(this.state.no_of_rows).forEach((index) => {
            let text_for_row = this.props.figure_list[index];
            let column = <Figure key={index.toString()} text_for_row={text_for_row}
                                 dispatcher={this.props.dispatcher}>
            </Figure>;

            rows_list.push(column);
        });

        return (
            <Paper zDepth={3} style={this.get_styles()}>
                {rows_list}
            </Paper>)
    }
}
class Figure extends React.Component {
    get_styles = () => {
        return ({
            color: 'red',
            border: 'dotted',
            flex: 1,
        })
    }

    constructor(props) {
        console.log("constructing row ");
        super(props);
        this.state = {ypos: _.random(-20, 0)};
        console.log('ypos' + this.state.ypos);
        console.log('text for row' + this.state.text_for_row);
        console.log("dispatcher2 == " + this.props.dispatcher);
    }

    componentDidMount = () => {
        console.log("componentDidMount ypos" + this.state.ypos);
    }


    render = () =>
        <div style={this.get_styles()}>
            <HBall text_for_row={this.props.text_for_row} ypos={this.state.ypos}
                   dispatcher={this.props.dispatcher}/>
        </div>
}

class HBall extends React.Component {
    constructor(props) {
        super(props);
        console.log("dispatcher3 == " + this.props.dispatcher);
        this.state = {visibility:'visible'}
    }


    hball_clicked = (e) => {
        console.log("hball value " + e.target.innerHTML);
        this.props.dispatcher.dispatch({
            type: 'change',
            letter: this.props.text_for_row
        });
        this.setState({visibility:'hidden'});
    }

    render = () => {
        let ypos_percentage = this.props.ypos + '%';
        return (
            <div>
                <div style={{color: 'red', visibility:this.state.visibility}}
                     onClick={this.hball_clicked}>
                    {this.props.text_for_row}
                </div>
            </div>
        )
    }
}

